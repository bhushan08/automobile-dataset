# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 18:06:05 2019

@author: BHUSHAN
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

url = 'https://archive.ics.uci.edu/ml/machine-learning-databases/autos/imports-85.data'



dataframe = pd.read_csv(url, header = None)


headers = ["symboling","normalized-losses","make","fuel-type","aspiration", "num-of-doors","body-style",
         "drive-wheels","engine-location","wheel-base", "length","width","height","curb-weight","engine-type",
         "num-of-cylinders", "engine-size","fuel-system","bore","stroke","compression-ratio","horsepower",
         "peak-rpm","city-mpg","highway-mpg","price"]

dataframe.columns = headers

dataframe.to_csv("automobiles.csv")


#droping the missing values rows
dataframe.dropna(subset = ['price'], axis = 0)

dataframe.replace('?', np.nan, inplace = True)

#identifying the missing data
missing_data = dataframe.isnull()

#countinf the missing data in every column 
for columns in missing_data.columns.values.tolist():
    print(missing_data[columns].value_counts())
    
    
#calculating the average normalised loss mean so that we can replace the empty rows with the mean
average_normalised_loss = dataframe['normalized-losses'].astype('float').mean(axis = 0)

#replacing the nan values or empty values with mean

dataframe['normalized-losses'].replace(np.nan, average_normalised_loss, inplace = True)

average_bore = dataframe['bore'].astype('float').mean(axis = 0)

dataframe['bore'].replace(np.nan, average_bore, inplace = True)

missing_values_array = ['peak-rpm', 'stroke', 'bore', 'horsepower']

#finding and replacing the missing values once
for values in missing_values_array:
    missing = dataframe[values].astype('float').mean(axis = 0)
    dataframe[values].replace(np.nan, missing, inplace = True)

#dropping the nan values in price column
dataframe.dropna(subset = ['price'], axis = 0, inplace = True)
dataframe.reset_index(drop = True, inplace = True)

#changing the datatypes of columns 
dataframe[['normalized-losses']] = dataframe[['normalized-losses']].astype('int')

dataframe[['bore', 'stroke', 'price', 'peak-rpm']] = dataframe[['bore', 'stroke', 'price', 'peak-rpm']].astype('float')

#converting the miles per galon to liters per kilometers
dataframe['city-L/100km'] =  235/dataframe['city-mpg']
dataframe['highway-L/100km'] =  235/dataframe['highway-mpg']

#dataframe.rename(columns = {'highway-L/100km': 'highway-mpg'}, inplace = True)

#normalizing the values of lenght height and width

dataframe['length'] = dataframe['length']/dataframe['length'].max()
dataframe['width'] = dataframe['width']/dataframe['width'].max()
dataframe['height'] = dataframe['height']/dataframe['height'].max()

dataframe['horsepower'] = dataframe['horsepower'].astype(int, copy = True)


plt.hist(dataframe['horsepower'])
plt.xlabel("horsepower")
plt.ylabel('counts')

#applying binning to the horsepower data set and splitting them into 3 parts low medium high 
#thats why in np.linspace(start_value, end_value, numbers_generated) number_generated will be 4
bins = np.linspace(min(dataframe['horsepower']), max(dataframe['horsepower']), 4)
group_names = ['low', 'medium', 'high']
dataframe['horsepower-binned'] = pd.cut(dataframe['horsepower'], bins, labels = group_names, include_lowest = True)


#pplotting the binned values 
plt.bar(group_names, dataframe['horsepower-binned'].value_counts())

#plotting histogram 
plt.hist(dataframe['horsepower'], bins= 3)

from sklearn.preprocessing import OneHotEncoder, LabelEncoder
labelEncoder = LabelEncoder()
dataframe['fuel-type'] = labelEncoder.fit_transform(dataframe['fuel-type'])
dataframe['aspiration'] = labelEncoder.fit_transform(dataframe['aspiration'])
#oneHotEncoder = OneHotEncoder(categorical_features= [3])
#dataframe['fuel-type'] = oneHotEncoder.fit_transform(dataframe['fuel-type'])
#dataframe['aspiration'] = oneHotEncoder.fit_transform(dataframe['aspiration'])


#for getting the correlation between bore stroke compression-ratio and horsepower
dataframe[['bore', 'stroke', 'compression-ratio', 'horsepower']].corr()

import seaborn as sns
sns.regplot(x = 'engine-size', y = 'price', data=dataframe)
plt.ylim(0,)

sns.regplot(x = 'highway-L/100km', y = 'price', data = dataframe)
plt.ylim(0,)


sns.boxplot(x = 'body-style', y = 'price', data=dataframe)

sns.boxplot(x = 'engine-location', y = 'price', data=dataframe)

sns.boxplot(x = 'drive-wheels', y = 'price', data= dataframe)

df_grouped_one = dataframe[['drive-wheels','body-style','price']]

df_grouped_one = df_grouped_one.groupby(['drive-wheels'],as_index = False).mean()

group_test = dataframe[['drive-wheels','body-style','price']]
group_test_1 = group_test.groupby(['drive-wheels','body-style'],as_index = False).mean()

grouped_pivot = group_test_1.pivot(index = 'drive-wheels', columns = 'body-style')

grouped_pivot = grouped_pivot.fillna(0)

group_test_2 = dataframe[['body-style','price']]
group_test_body = group_test_2.groupby(['body-style'], as_index = False).mean()

plt.pcolor(grouped_pivot, cmap =  'RdBu')
plt.colorbar()
plt.show()

drive_wheels_counts = dataframe['drive-wheels'].value_counts().to_frame()
drive_wheels_counts.rename(columns = {'drive-wheels':'value_counts'}, inplace = True)
drive_wheels_counts.index.name = 'drive-wheels'


engine_location_value_counts = dataframe['engine-location'].value_counts().to_frame()
engine_location_value_counts.rename(columns = {'engine-location':'value_counts'}, inplace = True)
engine_location_value_counts.index.name = 'engine-location'

#Calculating the pvalues of certain elements
#also correlation and causation

from scipy import stats

pearson_coeff, p_value = stats.pearsonr(dataframe['wheel-base'], dataframe['price'])

pearson_coeff, p_value = stats.pearsonr(dataframe['length'], dataframe['price'])

pearson_coeff, p_value = stats.pearsonr(dataframe['horsepower'], dataframe['price'])

pearson_coeff, p_value = stats.pearsonr(dataframe['width'], dataframe['price'])

pearson_coeff, p_value = stats.pearsonr(dataframe['curb-weight'], dataframe['price'])

pearson_coeff, p_value = stats.pearsonr(dataframe['engine-size'], dataframe['price'])

pearson_coeff, p_value = stats.pearsonr(dataframe['bore'], dataframe['price'])


#Analysis of Varience

grouped_by_drive_wheels = group_test[['drive-wheels','price']].groupby(['drive-wheels'])

grouped_by_drive_wheels.get_group('4wd')['price']

#comparing p_val and f_val and determine which has highest values
f_value, p_value = stats.f_oneway(grouped_by_drive_wheels.get_group('rwd')['price'], grouped_by_drive_wheels.get_group('fwd')['price'])
f_value, p_value = stats.f_oneway(grouped_by_drive_wheels.get_group('rwd')['price'], grouped_by_drive_wheels.get_group('4wd')['price'])
f_value, p_value = stats.f_oneway(grouped_by_drive_wheels.get_group('4wd')['price'], grouped_by_drive_wheels.get_group('fwd')['price'])

#Fitting the data in the model
#first we try with different features

#LINEAR REGRESSION MODEL

from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
x = dataframe[['highway-mpg']]
y = dataframe[['price']]

regressor.fit(x,y)

ypred = regressor.predict(x)

regressor.intercept_
regressor.coef_

print(38423.30585816-(821.73337832*27))

#MULTIPLE LINEAR REGRESSION MODEL

z = dataframe[['highway-mpg', 'horsepower','curb-weight','engine-size']]
regressor.fit(z, y)

ypred = regressor.predict(z)

regressor.intercept_
regressor.coef_

#Regression PLot
sns.regplot(x = 'highway-mpg', y = 'price', data= dataframe)
sns.regplot(x = 'peak-rpm', y = 'price', data=dataframe)
sns.regplot(x = 'engine-size', y = 'price', data= dataframe)

#Actual Vs Fitted or Predicted Values
axis_1 = sns.distplot(dataframe['price'], hist= False, color='red', label='Actual Value')
sns.distplot(ypred, hist=False, color='blue',label='Predicted Values', ax=axis_1)
plt.show()

#POLYNOMIAL LINEAR REGRESSION
#Creating a Fucntion to plot the data
def PolynomialPlot(model, independent_variable, dependent_variable, Name):
    x_new = np.linspace(15, 55, 100)
    y_new = model(x_new)
    
    plt.plot(independent_variable, dependent_variable,'.',x_new,y_new,'_')#'.','_' are markers for the plot in which the the data will be plotted
    plt.title("Polynomial fit in the Dataset")
    axis_ = plt.gca()
    axis_.set_facecolor((0.898, 0.898, 0.898))
    fig =plt.gcf()
    plt.xlabel(Name)
    plt.ylabel('Price of cars')
    plt.legend()
    
    plt.show()
    plt.close()
    
 
#fit the polynomial using the function polyfit,
#then use the function poly1d to display the polynomial function.
x = dataframe[['highway-mpg']]
y = dataframe[['price']]

f = np.polyfit(x,y, 3)
p = np.poly1d(f)

print(p)

PolynomialPlot(p, x, y ,'Highway-mpg')

#plotting 11th order polynomial
f1 = np.polyfit(x,y,11)
p1 = np.poly1d(f1)

print(p1)

PolynomialPlot(p1,x,y, '11th Orde plotting')

from sklearn.preprocessing import PolynomialFeatures

polynomial_regressor = PolynomialFeatures()
Z_polynomial_regressor = polynomial_regressor.fit_transform(z)

z.shape
Z_polynomial_regressor.shape

#Using Pipeline
#PIPELINE

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

Input=[('scale',StandardScaler()), ('polynomial', PolynomialFeatures(include_bias=False)), ('model',LinearRegression())]

pipe = Pipeline(Input)

pipe.fit(z,y)

y_pipe = pipe.predict(z)

regressor.fit(x,y)
regressor.score(x,y)#R-square for LINEAR REGRESSION

ypred = regressor.predict(x)

from sklearn.metrics import mean_squared_error
mse = mean_squared_error(dataframe['price'], ypred)#MSE FOR LINEAR REGRESSION

regressor.fit(z,dataframe['price'])
regressor.score(z, dataframe['price'])#R-square for MULTIPLE LINEAR REGRESSION

y_predict_multifit = regressor.predict(z)
mean_squared_error(dataframe['price'], y_predict_multifit)#MSE FOR MULTIPLE LINEAR REGRESSION









